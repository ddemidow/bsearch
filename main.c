#include <stdio.h>

int bsearch(int array[], int sz, int x)
{
	if ( sz > 0 ) {
		if ( x < array[sz-1] ) {
			int first = 0;
			int last = sz;
			while ( first < last ) {
				int middle = first + (last - first) / 2;
				if ( x < array[middle] )
					last = middle;
				else
					first = middle + 1;
			}
			return last;
		}
	}
	return -1;
}

#define SIZE 6

int main(void) {
	// your code goes here
	int array[SIZE] = {0, 1, 1, 2, 3, 4};
	int h = bsearch(array, SIZE, -1);
	printf("h(-1) = %d\n", h);
	h = bsearch(array, SIZE, 0);
	printf("h(0) = %d\n", h);
	h = bsearch(array, SIZE, 1);
	printf("h(1) = %d\n", h);
	h = bsearch(array, SIZE, 2);
	printf("h(2) = %d\n", h);
	h = bsearch(array, SIZE, 3);
	printf("h(3) = %d\n", h);
	h = bsearch(array, SIZE, 4);
	printf("h(4) = %d\n", h); // error!
	return 0;
}
